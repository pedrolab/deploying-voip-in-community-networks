Pedro, here you have some use cases and user stories we would like to have working. -- David.

## Voip mediator

We want all the voip traffic to be mediated by our services,
in order to flexibilize the services we offer.
So, instead of configuring users devices to point the external voip service,
we want them to point to our services in order to redirect them.

## Inner calls

We wants that calls within guifi.net network don't need to go out to a voip provider
and can be solved internally to save both bandwidth at the Internet gateways and provider fees.

## Gateway balancing

Having all the communications centralized inside the guifi.net zone,
has many flexibility but also many implies that the resources to go outside

## Receiving calls on multiple devices

A user wants to be able to receive calls for the same landline number
at his home and at his office, both connected to the guifi.net network.

**Considerations:** Calling from multiple devices is currently working,
but incoming calls are received just by the last device that registers on the voip provider.
This can be solved by contracting an extension for each device to the voip provider,
but the provider charges us more for each extension in a call group than for a single line.
So the idea is that an inner system maps single sip account to multiple devices.

## Mobile landline

A user wants to have a landline in his house but when she leaves,
she wants to be able to call from her smartphone using a voip app
wherever she has wifi available.

**Considerations:** This use case would work if we configure the app to directly use the voip provider.
But if we want to provied multiple receiving devices and need a mediator in the inner network,
this implies that:

- the mediator should be available from internet
- a call from outside the guifi.net network cost double in terms of gateway nodes bandwidth.

## Internal phone numbers

A user does not want to contract a voip landline.
Just to make inner calls to the community network,
but most devices have just numeric pads.


## Federation

Extending inner services to other guifi.net zones by federating them.

By interconnecting several services like ours, over different zones,
many of the services we offer would be more powerfull,
since they are not limited to our zone and many of the services
could be offered without going out the guifi.net network.

**Considerations**
This use case is maybe too wide and we could split it in many use cases:
the basic federation infrastructure, and then, how to use the different services over that federation.


## Encrypted calls

Given the recent cases of international communications tapping,
our clients want to keep the privacy of their comercial operations by encrypting their phone communications.

**Considerations:**
This use case could be limited to the features supported by the terminals and the voip provider when involved.


## Video calls


## Instant messaging


## User management interface

An operator wants to add a service for a given user.

**Considerations**
Depending on the used technology the way of adding a given service to a given user
implies to touch many configuration files in a very complexe ways.
Since the services are a limited set, and the changes are complex but repetitive,
we should abstract the operator a simple interface to add the service for the client.

In order to be addressed, this use case should be splitted into one for each kind of service.


## Integration with the billing platform

Since some services are paid, GuifiBaix needs an API to collect per-user traffic numbers.



To be continued...



