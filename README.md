Deploying VoIP in Community Networks
====================================

This project has been frozen, look at the latest entry in the diary

Give facilities to extend VoIP services in guifi.net

Plan
----

### Objectives
1. [**Software development**] Improve the VoIP solution that there is in Sant Joan Despí zone. Collaboration with http://guifibaix.coop/
2. [**Deployment**] Use the same system in the Community Network of Sant Andreu neighborhood. Study if is extensible for all Barcelona.
3. [**Documentation**] Make it extensible for all qMp networks in general, and guifi.net.
4. [**Migration IPv4->IPv6**] Study the integration with IPv6. Barcelona Community Network is making efforts to migrate to IPv6.

### Functionalities
1. [Free] Calls inside the guifi.net network
2. [Low cost] 1 + Calls to PSTN network.
3. [Shareable?] Can be the low cost option be shareable?

### Devices
- Computers
- VoIP telephones / Analog telephones with VoIP extension
- Mobile phones connected with wifi

### Precedents
- http://ddd.uab.cat/record/70278?ln=ca
